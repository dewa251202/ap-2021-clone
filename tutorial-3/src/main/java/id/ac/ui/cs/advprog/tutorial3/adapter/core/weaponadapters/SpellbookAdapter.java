package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private String latestSpellType;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.latestSpellType = "none";
    }

    @Override
    public String normalAttack() {
        latestSpellType = "small";
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(latestSpellType.equals("large")){
            latestSpellType = "none";
            return "Magic power not enough for large spell";
        }
        else {
            latestSpellType = "large";
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

    public Spellbook getSpellbook(){
        return this.spellbook;
    }
}
