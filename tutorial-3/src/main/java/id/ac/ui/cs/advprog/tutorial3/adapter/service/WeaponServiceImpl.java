package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> allWeapons = new ArrayList<>();

        for(Bow bow : bowRepository.findAll()){
            allWeapons.add(new BowAdapter(bow));
        }

        for(Spellbook spellbook : spellbookRepository.findAll()){
            allWeapons.add(new SpellbookAdapter(spellbook));
        }

        allWeapons.addAll(weaponRepository.findAll());

        return allWeapons;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        String adaptedWeaponType;
        Weapon weapon;
        if(bowRepository.findByAlias(weaponName) != null){
            adaptedWeaponType = "bow";
            weapon = new BowAdapter(bowRepository.findByAlias(weaponName));
        }
        else if(spellbookRepository.findByAlias(weaponName) != null){
            adaptedWeaponType = "spellbook";
            weapon = new SpellbookAdapter(spellbookRepository.findByAlias(weaponName));
        }
        else {
            adaptedWeaponType = "weapon";
            weapon = weaponRepository.findByAlias(weaponName);
        }

        String attackTypeStr;
        String attackDesc;
        if(attackType == 0){
            attackTypeStr = "normal attack";
            attackDesc = weapon.normalAttack();
        }
        else if(attackType == 1){
            attackTypeStr = "charged attack";
            attackDesc = weapon.chargedAttack();
        }
        else{
            attackTypeStr = "unknown attack";
            attackDesc = "<Let it be a mystery>";
        }

        logRepository.addLog(weapon.getHolderName() + " attacked with " + weapon.getName() +
                " (" + attackTypeStr + "): " + attackDesc);
        if(adaptedWeaponType == "bow"){
            bowRepository.save(((BowAdapter)weapon).getBow());
        }
        else if(adaptedWeaponType == "spellbook"){
            spellbookRepository.save(((SpellbookAdapter)weapon).getSpellbook());
        }
        else{
            weaponRepository.save(weapon);
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
