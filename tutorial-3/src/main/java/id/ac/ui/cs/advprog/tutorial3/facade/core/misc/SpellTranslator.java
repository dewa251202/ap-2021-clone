package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.*;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.*;

public class SpellTranslator {
    public static String encode(String text){
        // Step #1
        Spell spell = new Spell(text, AlphaCodex.getInstance());

        // Step #2
        spell = new CelestialTransformation().encode(spell);

        // Step #3
        spell = new AbyssalTransformation(text.length()/2).encode(spell);

        // Step #4
        spell = new TibbarkcajTransformation().encode(spell);

        // Step #5
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());

        return spell.getText();
    }

    public static String decode(String code){
        // Step #1
        Spell spell = new Spell(code, RunicCodex.getInstance());

        // Step #2
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());

        // Step #3
        spell = new TibbarkcajTransformation().decode(spell);

        // Step #4
        spell = new AbyssalTransformation(code.length()/2).decode(spell);

        // Step #5
        spell = new CelestialTransformation().decode(spell);

        return spell.getText();
    }
}
