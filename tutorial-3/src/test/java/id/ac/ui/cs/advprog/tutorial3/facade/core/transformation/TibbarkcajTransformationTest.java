package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TibbarkcajTransformationTest {
    private Class<?> tibbarkcajClass;

    @BeforeEach
    public void setup() throws Exception {
        tibbarkcajClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.TibbarkcajTransformation");
    }

    @Test
    public void testTibbarkcajHasEncodeMethod() throws Exception {
        Method translate = tibbarkcajClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTibbarkacjEncodesCorrectly() throws Exception {
        String text = "Dewangga Putra Sheradhien";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "lK3OBIIOpZ568OpWHK8OLHGKB";

        Spell result = new TibbarkcajTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testTibbarkcajHasDecodeMethod() throws Exception {
        Method translate = tibbarkcajClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testTibbarkcajDecodesCorrectly() throws Exception {
        String text = "lK3OBIIOpZ568OpWHK8OLHGKB";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Dewangga Putra Sheradhien";

        Spell result = new TibbarkcajTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
