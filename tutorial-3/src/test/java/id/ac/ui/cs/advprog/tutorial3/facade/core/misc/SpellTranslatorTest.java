package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.*;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpellTranslatorTest {
    @Test
    public void testSpellTranslatorHasStaticEncodeMethod() throws Exception {
        Class<?> translatorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.SpellTranslator");
        Method encode = translatorClass.getDeclaredMethod("encode", String.class);
        int methodModifiers = encode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testSpellTranslatorHasStaticDecodeMethod() throws Exception {
        Class<?> translatorClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.misc.SpellTranslator");
        Method decode = translatorClass.getDeclaredMethod("decode", String.class);
        int methodModifiers = decode.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isStatic(methodModifiers));
    }

    @Test
    public void testSpellEncodeProperly() throws Exception {
        String text = "Hes a jackrabbit hes a jackrabbit Go get him Hes jumping around";
        String expected = "ni/Fo:[^zH_(Xr*#K{MCNSG@d;*Hd%,p>Su,s|{_{A<})HaK!,HC[u(>?!S>_.%";

        String result = SpellTranslator.encode(text);
        assertEquals(expected, result);
    }

    @Test
    public void testSpellDecodeProperly() throws Exception {
        String code = "ni/Fo:[^zH_(Xr*#K{MCNSG@d;*Hd%,p>Su,s|{_{A<})HaK!,HC[u(>?!S>_.%";
        String expected = "Hes a jackrabbit hes a jackrabbit Go get him Hes jumping around";

        String result = SpellTranslator.decode(code);
        assertEquals(expected, result);
    }
}
