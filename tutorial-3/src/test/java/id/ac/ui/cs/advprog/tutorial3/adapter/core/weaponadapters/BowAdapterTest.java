package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
@ExtendWith(MockitoExtension.class)
public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Mock
    Bow mockBow;

    @InjectMocks
    BowAdapter bowAdapter = new BowAdapter(mockBow);

    @Test
    public void testBowAdapterHasGetIsAimShotMethod() throws Exception {
        Method getIsAimShot = bowAdapterClass.getDeclaredMethod("getIsAimShot");

        assertEquals("boolean",
                getIsAimShot.getGenericReturnType().getTypeName());
        assertEquals(0,
                getIsAimShot.getParameterCount());
        assertTrue(Modifier.isPublic(getIsAimShot.getModifiers()));
    }

    @Test
    public void testBowAdapterHasGetBowMethod() throws Exception {
        Method getBow = bowAdapterClass.getDeclaredMethod("getBow");

        assertEquals("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow",
                getBow.getGenericReturnType().getTypeName());
        assertEquals(0,
                getBow.getParameterCount());
        assertTrue(Modifier.isPublic(getBow.getModifiers()));
    }

    @Test
    public void testNormalAttackShouldCallShootArrowOfAdaptee() throws Exception {
        bowAdapter.normalAttack();
        verify(mockBow).shootArrow(bowAdapter.getIsAimShot());
    }

    @Test
    public void testChargedAttackShouldSwitchAttackMode() throws Exception {
        boolean isAimShot = bowAdapter.getIsAimShot();
        bowAdapter.chargedAttack();
        assertEquals(!isAimShot, bowAdapter.getIsAimShot());

        isAimShot = bowAdapter.getIsAimShot();
        bowAdapter.chargedAttack();
        assertEquals(!isAimShot, bowAdapter.getIsAimShot());
    }

    @Test
    public void testGetNameShouldCallGetNameOfAdaptee() throws Exception {
        bowAdapter.getName();
        verify(mockBow).getName();
    }

    @Test void testGetHolderNameShouldCallGetHolderNameOfAdaptee() throws Exception {
        bowAdapter.getHolderName();
        verify(mockBow).getHolderName();
    }
}
